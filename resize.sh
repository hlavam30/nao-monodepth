#!/bin/sh

# usage: ./resize.sh [working_directory] [files_glob]

# example: ./resize.sh data/walk "*.jpg"

cd $1

# resized and copies images to _resized folder
find . -maxdepth 2 -iname $2 | xargs -L1 -I{} convert -resize 512x256\! "{}" _resized/"{}"

# copies the filename.txt file to _resized folder
find . -maxdepth 2 -iname "filenames.txt" | xargs cp "{}" _resized/"{}"