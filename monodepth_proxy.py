import sys
import numpy as np
import argparse
import re
import tensorflow as tf
import tensorflow.contrib.slim as slim
import scipy.misc
import matplotlib.pyplot as plt

sys.path.append('monodepth')
from monodepth_model import *
from monodepth_dataloader import *
from average_gradients import *

def post_process_disparity(disp):
    _, h, w = disp.shape
    l_disp = disp[0,:,:]
    r_disp = np.fliplr(disp[1,:,:])
    m_disp = 0.5 * (l_disp + r_disp)
    l, _ = np.meshgrid(np.linspace(0, 1, w), np.linspace(0, 1, h))
    l_mask = 1.0 - np.clip(20 * (l - 0.05), 0, 1)
    r_mask = np.fliplr(l_mask)
    return r_mask * l_disp + l_mask * r_disp + (1.0 - l_mask - r_mask) * m_disp

def create_parameters():
    return monodepth_parameters(
        encoder='vgg',
        height=480,
        width=640,
        batch_size=2,
        num_threads=1,
        num_epochs=1,
        do_stereo=False,
        wrap_mode='border',
        use_deconv=False,
        alpha_image_loss=0,
        disp_gradient_loss_weight=0,
        lr_loss_weight=0,
        full_summary=False)

def init(params, inputParams):
    left  = tf.placeholder(tf.float32, [2, inputParams['input_height'], inputParams['input_width'], 3])
    model = MonodepthModel(params, "test", left, None)

    config = tf.ConfigProto(allow_soft_placement=True)
    sess = tf.Session(config=config)

    train_saver = tf.train.Saver()

    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())
    coordinator = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coordinator)

    restore_path = inputParams['checkpoint_path']
    train_saver.restore(sess, restore_path)

    return (sess, model)

def run(frame, sess, model, params, inputParams):
    left = model.left

    input_image = frame.astype(np.float32) / 255
    input_images = np.stack((input_image, np.fliplr(input_image)), 0)

    disp = sess.run(model.disp_left_est[0], feed_dict={left: input_images})
    disp_pp = post_process_disparity(disp.squeeze()).astype(np.float32)

    return disp_pp