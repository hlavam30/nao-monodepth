# -*- coding: utf-8 -*-
import os
import sys
import time
from naoqi import ALProxy
import argparse
import monodepth_proxy as monodepth
import numpy as np
import scipy.misc
import matplotlib
import matplotlib.pyplot as plt

dprefix = '⬛️🔶 '

frame_count = 0

plt.switch_backend("TkAgg")
matplotlib.rcParams['figure.figsize'] = 20,10

parser = argparse.ArgumentParser(description='NAO Monodepth')

parser.add_argument('--ip', type=str, help='ip address of the robot', required=True)
parser.add_argument('--port', type=int, help='port of the robot', default=9559)
parser.add_argument('--saveprevs', type=str, help='saves previews with prefix, leave empty to disable', default='')
parser.add_argument('--model', type=str, help='pretrained model to use', default='kitti')

args = parser.parse_args()

def connect_robot():
    proxy = ALProxy("ALVideoDevice", args.ip, args.port)
    proxy.unsubscribeAllInstances("cam")
    name = "cam"
    camID = 0
    resolution = 1
    colorSpace = 13
    fps = 30

    return (proxy, proxy.subscribeCamera(name, camID, resolution, colorSpace, fps))

def get_frame(proxy, cam, w, h):
    image = proxy.getImageRemote(cam)

    im = image[6]
    nparr = np.fromstring(im, np.uint8)
    nparr = nparr.reshape(240,320,3)
    nparr = scipy.misc.imresize(nparr, [h, w], interp='lanczos')

    plt.subplot(1, 2, 1)
    plt.imshow(nparr)

    return nparr

def mock_frame(filename, w, h):
    input_image = scipy.misc.imread(filename, mode="RGB")
    input_image = scipy.misc.imresize(input_image, [h, w], interp='lanczos')

    plt.subplot(1, 2, 1)
    plt.imshow(input_image)

    return input_image

print(dprefix + '🔧 Initializing monodepth...')

params = monodepth.create_parameters()

inputParams = {
    'input_width': 512,
    'input_height': 256,
    'checkpoint_path': 'monodepth/models/model_%s/model_%s' % (args.model, args.model)
}

print(dprefix + '📕 Using pretrained model "%s"' % inputParams['checkpoint_path'])

# Initialize 
(sess, model) = monodepth.init(params, inputParams)
print(dprefix + '☑️  Monodepth ready.')

# Connect to robot
print(dprefix + '🤖 Connecting to NAO...')
(proxy, cam) = connect_robot()

def detect_corridor (disp, height):
    dispt = np.transpose(disp)
    sums = []

    for col in dispt:
        sum = 0
        for r in range(1, height):
            sum += col[r] - col[r-1] if col[r] - col[r-1] > 0 else (col[r] - col[r-1]) * 10
        sums.append(sum)

    chunk_width = 32

    sums_chunks = np.reshape(np.array(sums), (-1, chunk_width))
    sums_chunks = np.average(sums_chunks, axis=1).tolist()
    print(sums_chunks)

    max_idx = sums_chunks.index(max(sums_chunks))

    print(max(sums_chunks))

    for i in range(3):
        dispt[max_idx*chunk_width + i] = np.array(map(lambda x: 0.05, dispt[max_idx*chunk_width]))
        dispt[max_idx*chunk_width + i+3] = np.array(map(lambda x: 0, dispt[max_idx*chunk_width]))
        dispt[max_idx*chunk_width + i + chunk_width] = np.array(map(lambda x: 0, dispt[max_idx*chunk_width]))
        dispt[max_idx*chunk_width + i+3 + chunk_width] = np.array(map(lambda x: 0.05, dispt[max_idx*chunk_width]))


    disp = np.transpose(dispt)

    return disp

while True:
    print(dprefix + '🤖📸 Getting frame %d from NAO...' % frame_count)
    # Get frame
    #frame = mock_frame('data/original/corridor/frame_left_24.jpg', inputParams['input_width'], inputParams['input_height'])
    frame = get_frame(proxy, cam, inputParams['input_width'], inputParams['input_height'])

    print(dprefix + '🖥 ⌛️ Computing frame %d...' % frame_count)
    # Run the session
    disp = monodepth.run(frame, sess, model, params, inputParams)
    
    disp = detect_corridor(disp, inputParams['input_height'])

    # Display as picture
    plt.subplot(1, 2, 2)
    plt.imshow(disp, cmap='plasma')
    plt.pause(0.1)

    if len(args.saveprevs) > 0:
        outfname = 'previews/%s-%d.png' % (args.saveprevs, frame_count)
        plt.savefig(outfname)

    frame_count += 1